import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Store } from './store';

@Injectable()
export class StoreDataService {

  private storesUrl = 'http://milenabooks.azurewebsites.net/api/Store';
  private storeBookUrl = 'http://milenabooks.azurewebsites.net/api/store/';
  private deleteStoreUrl = 'http://milenabooks.azurewebsites.net/api/store/';
  private updateStoreUrl = 'http://milenabooks.azurewebsites.net/api/store/';
  constructor(private http: HttpClient) { }

  getStores(): Observable<Store[]> {
    return this.http.get<Store[]>(this.storesUrl);
  }
  getStore(id): Observable<Store> {
    const url = `${this.storesUrl}/${id}/books`;
    return this.http.get<Store>(url);
  }
  deleteStore(id) {
    const url = `${this.deleteStoreUrl}/${id}`;
    return this.http.delete(url);
  }
  updateStore(id, data) {
    const url = `${this.updateStoreUrl}/${id}`;
    return this.http.put(url, data);
  }
}

import { StoreDataService } from './../store-data.service';
import { Component, OnInit } from '@angular/core';
import { Store } from '../store';


@Component({
  selector: 'app-store-home',
  templateUrl: './store-home.component.html',
  styleUrls: ['./store-home.component.css']
})
export class StoreHomeComponent implements OnInit {

  public stores: Store[];
  constructor(private service: StoreDataService) { }

  ngOnInit() {
    this.getStores();

  }
  getStores() {
    this.service.getStores().subscribe(res => this.stores = res);
  }

}

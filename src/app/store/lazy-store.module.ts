import { StoreDataService } from './store-data.service';
import { StoreHomeComponent } from './store-home/store-home.component';
import { StoreDetailComponent } from './store-detail/store-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
const storeRoutes: Routes = [
  { path: '', component: StoreHomeComponent },
  { path: 'store/:id', component: StoreDetailComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(storeRoutes),
    NgbModule,
    FormsModule
  ],
  declarations: [
    StoreHomeComponent,
    StoreDetailComponent
  ],
  providers: [StoreDataService]
})
export class LazyStoreModule { }

import { Book } from './../../books/shared/book';
import { StoreDataService } from './../store-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.css']
})
export class StoreDetailComponent implements OnInit {

  public storeBooks: any;
  private storeId: number;
  public display = false;
  public text = 'Update Store';
  public Name: string;
  public mr: NgbModalRef;
  @ViewChild('f') form: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: StoreDataService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.fetchStore();
  }
  fetchStore() {
    const id = + this.route.snapshot.paramMap.get('id');
    this.storeId = id;
    this.service.getStore(id).subscribe(res => {
      this.storeBooks = res;
    });
  }
  delete() {
    this.service.deleteStore(this.storeId).subscribe(res => this.router.navigate(['/stores']));

  }
  update() {
    this.display = !this.display;
    if (this.display) {
      this.text = 'Close Form';
    } else {
      this.text = 'Update Store';
    }
  }
  updateName() {
    this.service.updateStore(this.storeId, this.form.value).subscribe(res => this.router.navigate(['/stores']));
  }
  openModal(myModal) {
    this.mr = this.modalService.open(myModal);
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { BooksService } from '../shared/books.service';
import { Output, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Book } from '../shared/book';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public books: Book[];
  public model: any;
  constructor(private service: BooksService,
    public activeModal: NgbActiveModal,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.getBooks();
  }
  getBooks() {
    this.service.getBooks().subscribe(res => this.books = res);
  }

  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.books.filter(v => v.Name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  selectItem(event) {
    console.log(event);
    this.router.navigate(['/book-detail/', event.item.Id]);
    this.activeModal.close();
  }
  formatter = (value: any) => value.Name;
}



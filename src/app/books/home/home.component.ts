import { Component, OnInit } from '@angular/core';
import { BooksService } from '../shared/books.service';
import { Router } from '@angular/router';
import { Book } from '../shared/book';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Add types.

  public books: Book[];
  public recentlyBooks: Book[];
  public itemsPerPage = 12;
  public totalItems: number;
  public page = 1;
  public previousPage: any;
  constructor(
    private service: BooksService,
    private router: Router,
    private config: NgbRatingConfig) {
    config.max = 5;
    config.readonly = true;
  }

  ngOnInit() {
    this.getBooks();
    this.recentlyBooks = this.service.booksArray;
  }
  getBooks() {
    this.service.getBooks().subscribe(res => {
      this.totalItems = res.length;
      this.books = res;
    });
  }

  loadPage(page) {
    this.router.navigate(['/home'], { queryParams: { page: page } });
  }
  storedBooks(data) {
    this.service.booksArray.push(data);
  }

  recentBook(data) {
    this.service.booksArray.push(data);
  }
}

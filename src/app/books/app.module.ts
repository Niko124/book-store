import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { BooksService } from './shared/books.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AppRoutingModule, routingComponents } from './/app-routing.module';
import { DefaultPipe } from './shared/default.pipe';
import { SearchComponent } from './search/search.component';
import { FilterPipe } from './shared/filter.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RatingPipe } from './shared/rating.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    routingComponents,
    HomeComponent,
    DefaultPipe,
    SearchComponent,
    FilterPipe,
    RatingPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot()
  ],
  entryComponents: [SearchComponent],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }

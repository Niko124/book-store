import { AddBookComponent } from './add-book/add-book.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { BestBooksComponent } from './best-books/best-books.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'book-detail/:id', component: BookDetailComponent },
  { path: 'add-book/:id', component: AddBookComponent },
  { path: 'home', component: HomeComponent },
  { path: 'best', component: BestBooksComponent },
  { path: 'add-book', component: AddBookComponent },
  { path: 'stores', loadChildren: '../store/lazy-store.module#LazyStoreModule' },
  { path: '**', component: PageNotFoundComponent }

];
@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent, BookDetailComponent,
  PageNotFoundComponent, AddBookComponent, BestBooksComponent];

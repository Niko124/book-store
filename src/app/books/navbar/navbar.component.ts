import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchComponent } from '../search/search.component';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public isNavbarCollapsed = true;
  constructor(private modalService: NgbModal ) { }
  ngOnInit() {
  }
  open() {
    const modalRef = this.modalService.open(SearchComponent);
  }


}

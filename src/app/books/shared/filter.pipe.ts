import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(book: any, searchBook: any): any {
    if (searchBook === '') {
      return [];
    }
    return book.filter(elem => elem.Name.toLowerCase().includes(searchBook.toLowerCase()));
  }

}

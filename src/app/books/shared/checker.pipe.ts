import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'checker'
})
export class CheckerPipe implements PipeTransform {

  transform(value: string, fallback: string): string {
    let text = '';
    if (value === null) {
      text = fallback;
    } else {
      text = value;
    }
    return text;
  }

}

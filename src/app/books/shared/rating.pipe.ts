import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rating'
})
export class RatingPipe implements PipeTransform {

  transform(value: any, fallback: number): any {
    let extraRate = 0;
    console.log(value + '....' + fallback);
    if (value < 5) {
      extraRate = fallback;
    }
    return extraRate;
  }
}

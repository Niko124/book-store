import { Book } from './book';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Injectable()
export class BooksService {

  minRating = 4;
  booksArray = [];
  url = 'http://milenabooks.azurewebsites.net/api/';

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    const url = `${this.url}/Books`;
    return this.http.get<Book[]>(url);
  }
  getBook(id: number): Observable<Book> {
    const url = `${this.url}/Books/${id}`;
    return this.http.get<Book>(url);
  }
  deleteBook(id: number): Observable<Book> {
    const url = `${this.url}/Books/${id}`;
    return this.http.delete<Book>(url);
  }
  postBook(book) {
    const url = `${this.url}/Books`;
    return this.http.post(url, book);
  }
  putBook(book): Observable<Book> {
    const url = `${this.url}/Books/${book.Id}`;
    return this.http.put<Book>(url, book);
  }
  bestBooks() {
    return this.getBooks().map(data => data.filter(respond => respond.Rating > this.minRating));
  }
  postStore(store) {
    const url = `${this.url}/Store/`;
    return this.http.post(url, store);
  }
  postStoreBook(storeBook) {
    const url = `${this.url}/store/book`;
    return this.http.post(url, storeBook);
  }
}

export class Book {
  Id?: number = null;
  Name: string;
  Price: number;
  Author: string;
  PictureURL: string;
  Rating: number;
}

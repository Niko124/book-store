import { BooksService } from './../shared/books.service';
import { Component, OnInit } from '@angular/core';
import { Book } from '../shared/book';
import { trigger, state, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-best-books',
  templateUrl: './best-books.component.html',
  styleUrls: ['./best-books.component.css'],
  animations: [
    trigger('fade', [
      transition('void=>*', [
        style({ backgroundColor: '#FDF3E7', opacity: 0 }),
        animate(1500, style({ backgroundColor: 'white', opacity: 1 }))
      ])
    ])
  ]
})
export class BestBooksComponent implements OnInit {

  public books: Book[]; // add type.
  constructor(private service: BooksService) { }

  ngOnInit() {
    this.getBooks();
  }
  getBooks() {
    this.service.bestBooks().subscribe(data => this.books = data.slice(0, 10));
  }
}

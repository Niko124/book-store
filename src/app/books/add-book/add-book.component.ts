import { Book } from './../shared/book';
import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from '../shared/books.service';
import { StoreModel } from '../shared/store';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  // TODO add reactive form and unsubscribe on destroy.
  // TODO separate book and store form into components and bind their classes through input property.

  @ViewChild('f') bookForm: any;
  @ViewChild('f2') storeForm: any;

  public model: Book = new Book();
  public storeModel: StoreModel = new StoreModel();
  public uploads: string[] = [
    'Post Store',
    'Post Store Book'
  ];
  public selected = 'Book';
  public chosenOption: string = this.uploads[0];
  public storeName;

  constructor(
    private route: ActivatedRoute,
    private service: BooksService,
  ) {

  }

  ngOnInit() {
    this.getBook();
  }
  getBook(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id) {
      this.service.getBook(id).subscribe(res => {
        this.model = res;
      });
    }
  }
  submitBooks() {
    if (this.bookForm.valid && this.model.Id === null) {
      this.service.postBook(this.bookForm.value).subscribe();
      this.bookForm.reset();
    } else {
      this.service.putBook(this.bookForm.value).subscribe();
      this.bookForm.reset();
    }
  }
  submitStore() {
    if (this.chosenOption === 'Post Store') {
      this.service.postStore(this.storeForm.value).subscribe();
      this.storeForm.reset();
    } else {
      this.service.postStoreBook(this.storeForm.value).subscribe();
      this.storeForm.reset();
    }
  }
}

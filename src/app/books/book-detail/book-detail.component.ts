import { Book } from './../shared/book';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BooksService } from '../shared/books.service';
import { Location } from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  public book = new Book();
  private bookId: number;
  public mr: NgbModalRef;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: BooksService,
    private location: Location,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getBook();
  }

  getBook(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = +params.get('id');
      this.bookId = id;
      this.service.getBook(id).subscribe(res => {
        this.book = res;
      });
    });
  }
  locationBack(): void {
    this.location.back();
  }
  deleteBook() {
    this.closeModal();
    this.service.deleteBook(this.bookId).subscribe(data => this.router.navigate(['/home']));

  }
  openModal(myModal) {
    this.mr = this.modalService.open(myModal);
  }
  closeModal() {
    this.mr.close();
  }
}
